# frozen_string_literal: true

require 'zlib'

module Syntech
  module Import
    module Processor
      class Line
        include Sidekiq::Worker

        def perform(attachment_id, resource_name, index, row)
          @klass = Syntech::Import.resources[resource_name].to_class
          @row = row
          @instance = @klass.new(attributes)

          if @klass.where(crc32: crc).exists?
            @klass.where(crc32: crc).update(
              syntech_import_attachment_id: attachment_id,
              line: index
            )
          else
            @instance.syntech_import_attachment_id = attachment_id
            @instance.line = index
            @instance.crc32 = crc
            @instance.save!
          end
        end

        def attributes
          @attributes ||= @klass.importer_mapping
                                .map { |key, index| [key, @row[index]] }
                                .to_h
        end

        def crc
          Zlib.crc32 Hash[@instance.importer_atributes.sort].to_s
        end
      end
    end
  end
end
