# frozen_string_literal: true

module Syntech
  module Import
    module Processor
      class Excel
        include Sidekiq::Worker

        def perform(attachment_id)
          @attachment = Attachment.find(attachment_id)

          each_row do |row, index|
            Line.perform_async(attachment_id, @attachment.resource, index, row)
          end
        end

        def each_row
          with_tempfile do |file|
            excel = Saxlsx::Workbook.new file
            rows = excel.sheets.first.rows

            rows.each_with_index do |row, index|
              if index.zero?
                @headers = row
                next
              else
                yield row_hash(row), index
              end
            end
          end
        end

        def with_sheet
          with_tempfile do |file|
            excel = Saxlsx::Workbook.new file
            rows = excel.sheets.first.rows

            rows.each_with_index do |row, index|
              yield row, index
            end
          end
        end

        def row_hash(row)
          @headers.zip(row).to_h
        end

        def with_tempfile
          tempfile = Tempfile.new(['import_temp', '.xlsx'], encoding: 'ascii-8bit')
          tempfile.write(@attachment.file.download)
          tempfile.rewind

          yield tempfile
        ensure
          tempfile.close
          tempfile.unlink
        end
      end
    end
  end
end
