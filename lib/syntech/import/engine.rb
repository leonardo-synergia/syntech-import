# frozen_string_literal: true

require 'syntech/import/routes'
require 'syntech/import/models'
require 'syntech/import/processor/excel'
require 'syntech/import/processor/line'

module Syntech
  module Import
    class Engine < ::Rails::Engine
      isolate_namespace Syntech::Import

      config.generators do |g|
        g.test_framework :rspec
      end

      ActiveSupport.on_load(:active_record) do
        include Syntech::Import::Models
      end

      ActiveSupport.on_load(:action_controller) do
        include Syntech::Import::Controllers::UrlHelpers
      end
  
      ActiveSupport.on_load(:action_view) do
        include Syntech::Import::Controllers::UrlHelpers
      end
    end
  end
end
