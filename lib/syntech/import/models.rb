# frozen_string_literal: true

module Syntech
  module Import
    module Models
      extend ActiveSupport::Concern

      class_methods do
        def importer(*args)
          options = args.extract_options!
          cattr_accessor :importer_mapping
          self.importer_mapping = options[:mapping]

          send(:audited, except: %i[attachment_id line crc])
          send(:belongs_to, :attachment,
               class_name: '::Syntech::Import::Attachment',
               foreign_key: :syntech_import_attachment_id)
        end
      end

      def importer_atributes
        attributes.with_indifferent_access.slice(*importer_mapping.keys)
      end
    end
  end
end
