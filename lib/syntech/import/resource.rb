# frozen_string_literal: true

module Syntech
  module Import
    class Resource
      attr_reader :name

      def initialize(name, options = {})
        @name = name.to_s
        @options = options
        @class_name = @name.classify
      end

      def to_class
        @klass ||= @class_name.constantize
      rescue NameError
        raise NameError, <<~MSG.squish
          You defined a importer resource called :#{@name},
          but we coudn't find any object called #{@class_name}
        MSG
      end

      def to_s
        @name
      end
    end
  end
end
