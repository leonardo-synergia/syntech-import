# frozen_string_literal: true

module Syntech
  module Import
    module Controllers
      # Create url helpers to be used with resources on views
      #
      #   importer_path(:data) => data_path
      #   new_importer_path(:data) => new_data_path
      #
      # Those helpers are included by default to ActionController::Base.
      module UrlHelpers
        def new_importer_path(resource)
          main_app.send("new_#{resource}_path")
        end

        def importer_path(object)
          if object.respond_to? :id
            main_app.send("#{object.resource}_path", object.id)
          else
            main_app.send("#{object.to_s.pluralize}_path")  
          end
        end
      end
    end
  end
end
