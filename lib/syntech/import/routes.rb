# frozen_string_literal: true

module ActionDispatch::Routing
  class Mapper
    def importer(*resources)
      options = resources.extract_options!

      resources.map(&:to_sym).each do |name|
        resource = Syntech::Import.add_resource(name, options)

        importer_scope resource do
          importer_resource(resource)
        end
      end
    end

    protected

    def importer_resource(resource)
      scope :importer do
        resources resource.name, controller: 'syntech/import/importer', except: %i[edit update]
      end
    end

    def importer_scope(resource)
      constraint = lambda do |request|
        request.env['syntech.import.resource'] = resource
        true
      end

      constraints(constraint) do
        yield
      end
    end

  end
end
