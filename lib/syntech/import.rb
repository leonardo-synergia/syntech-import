# frozen_string_literal: true

require 'syntech/import/engine'
require 'syntech/import/resource'

module Syntech
  module Import
    module Controllers
      autoload :UrlHelpers, 'syntech/import/controllers/url_helpers'
    end

    mattr_reader :resources
    @@resources = {}

    def self.add_resource(resource, options)
      Resource.new(resource, options).tap do |res|
        @@resources[res.name] = res
      end
    end
  end
end
