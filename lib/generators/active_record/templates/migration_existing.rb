# frozen_string_literal: true

class AddImporterTo<%= table_name.camelize %> < ActiveRecord::Migration<%= migration_version %>
  def self.up
    change_table :<%= table_name %> do |t|
<% attributes.each do |attribute| -%>
      t.<%= attribute.type %> :<%= attribute.name %>
<% end -%>

<%= migration_data -%>
    end
  end

  def self.down
    # By default, we don't want to make any assumption about how to roll
    # back a migration when your model already exists. Please edit below
    # which fields you would like to remove in this migration.
    raise ActiveRecord::IrreversibleMigration
  end
end
