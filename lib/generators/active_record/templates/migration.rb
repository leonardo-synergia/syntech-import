# frozen_string_literal: true

class ImporterCreate<%= table_name.camelize %> < ActiveRecord::Migration<%= migration_version %>
  def change
    create_table :<%= table_name %> do |t|
<% attributes.each do |attribute| -%>
      t.<%= attribute.type %> :<%= attribute.name %>
<% end -%>

<%= migration_data -%>

      t.timestamps null: false
    end
  end
end
