# frozen_string_literal: true

require 'rails/generators/active_record'

module ActiveRecord
  module Generators
    class ImportGenerator < ActiveRecord::Generators::Base
      argument :attributes, type: :array, default: [], banner: "field:type field:type"

      source_root File.expand_path("../templates", __FILE__)

      def define_migration
        if invoke_with_model? || revoke_with_migration?
          template_file = "migration_existing.rb"
          template_name = "add_importer_to_"
        else
          template_file = "migration.rb"
          template_name = "importer_create_"
        end

        migration_template template_file,
                           "db/migrate/#{template_name}#{table_name}.rb",
                           migration_version: migration_version
      end

      def generate_model
        return if invoke_with_model?

        invoke "active_record:model", [name], migration: false 
      end

      def inject_content_on_model
        content = <<~CONTENT
          importer mapping: {
            # Place here your mappings in the format:
            # attribute: 'excel_column_name'
          }

        CONTENT

        class_path = namespaced? ? class_name.to_s.split("::") : [class_name]
        indent_depth = class_path.size
        content = content.split("\n").map { |line| "  " * indent_depth + line }
        content = content.join("\n") << "\n"

        inject_into_class(model_path, class_path.last, content) if model_exists?
      end

      private

      def invoke_with_model?
        behavior == :invoke && model_exists?
      end

      def revoke_with_migration?
        behavior == :revoke && migration_exists?(table_name)
      end

      def model_exists?
        File.exist?(File.join(destination_root, model_path))
      end
  
      def migration_exists?(table_name)
        Dir.glob("#{File.join(destination_root, db_migrate_path)}/[0-9]*_*.rb")
           .grep(/\d+_add_importer_to_#{table_name}.rb$/)
           .first
      end

      def model_path
        @model_path ||= File.join("app", "models", "#{file_path}.rb")
      end

      def migration_version
        "[#{Rails::VERSION::MAJOR}.#{Rails::VERSION::MINOR}]"
      end
 
      def migration_data
<<-RUBY
      t.integer :line
      t.string :crc32, index: true
      t.references :syntech_import_attachment, foreign_key: true
RUBY
      end

    end
  end
end
