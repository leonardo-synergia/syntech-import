# frozen_string_literal: true

require 'rails/generators/base'

module Syntech
  module Import
      class InstallGenerator < ::Rails::Generators::Base
        source_root File.expand_path('.')

        desc "Install Syntech Importer into your application."

        def copy_locale
          copy_file "config/locales/syntech_import.en.yml", "config/locales/syntech_import.en.yml"
        end
    end
  end
end
