# frozen_string_literal: true

require 'rails/generators/named_base'

module Syntech
  module Import
    class ImportGenerator < Rails::Generators::NamedBase
      namespace "syntech:import"
      source_root File.expand_path(".")

      desc <<~DESC.squish
        Generates a model with the given NAME (if one does not exist) with
        the importer configuration plus a migration file and it's routes.
      DESC

      hook_for :orm

      def add_devise_routes
        route "importer :#{plural_name}\n"
      end
    end
  end
end
