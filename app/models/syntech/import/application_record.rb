# frozen_string_literal: true

module Syntech
  module Import
    class ApplicationRecord < ActiveRecord::Base
      self.abstract_class = true
    end
  end
end
