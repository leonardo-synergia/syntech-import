# frozen_string_literal: true

module Syntech
  module Import
    class Attachment < ApplicationRecord
      validates :resource, presence: true

      has_one_attached :file

      scope :by_resource, ->(resource) { where(resource: resource.name) }

      def resource=(value)
        super(value.to_s)
      end
    end
  end
end
