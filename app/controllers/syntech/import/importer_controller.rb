# frozen_string_literal: true

require_dependency 'syntech/import/application_controller'

module Syntech
  module Import
    class ImporterController < ApplicationController
      def index
        @attachment = Attachment.new(resource: resource)
        @attachments = Attachment.by_resource(resource)
      end

      def create
        attachment = Attachment.new(resource_params.merge(resource: resource))
        if attachment.save
          Processor::Excel.perform_async(attachment.id)

          redirect_to importer_path(resource), notice: 'Base importada com sucesso'
        else
          redirect_to importer_path(resource), error: 'Erro ao importar a base'
        end
      end

      private

      def resource_params
        params.require(:attachment).permit(:file)
      end
    end
  end
end
