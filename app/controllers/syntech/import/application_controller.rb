# frozen_string_literal: true

module Syntech
  module Import
    class ApplicationController < ::ApplicationController
      protect_from_forgery with: :exception

      helper_method :resource

      private

      def resource
        @resource ||= request.env['syntech.import.resource']
      end
    end
  end
end
