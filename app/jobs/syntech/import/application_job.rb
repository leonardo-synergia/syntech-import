# frozen_string_literal: true

module Syntech
  module Import
    class ApplicationJob < ActiveJob::Base
    end
  end
end
