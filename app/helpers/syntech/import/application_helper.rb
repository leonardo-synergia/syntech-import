# frozen_string_literal: true

module Syntech
  module Import
    module ApplicationHelper

      def method_missing(method, *args, &block)
        if method.to_s =~ /_(path|url)/ && main_app.respond_to?(method)
          main_app.send(method, *args)
        else
          super
        end
      end

    end
  end
end
