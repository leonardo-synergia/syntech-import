# frozen_string_literal: true

class CreateSyntechImportAttachments < ActiveRecord::Migration[6.0]
  def change
    create_table :syntech_import_attachments do |t|
      t.string :resource
      t.string :status

      t.timestamps
    end
  end
end
