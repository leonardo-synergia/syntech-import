Rails.application.routes.draw do
  importer :donation, :indemnity

  root 'importer#donation'
end
