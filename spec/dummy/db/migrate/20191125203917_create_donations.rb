class CreateDonations < ActiveRecord::Migration[6.0]
  def change
    create_table :donations do |t|
      t.string :name
      t.datetime :donated_at
      t.decimal :value, precision: 10, scale: 2

      t.timestamps
    end
  end
end
